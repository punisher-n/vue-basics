let count = 0;
const countsHeader = document.getElementById('counts')
const addBtn = document.getElementById('add')
const minusBtn = document.getElementById('minus')

countsHeader.innerText = count
addBtn.addEventListener("click", () => {
    count += 1
    countsHeader.innerText = count
})

minusBtn.addEventListener("click", () => {
    count -= 1
    countsHeader.innerText = count
})